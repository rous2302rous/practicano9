package ito.poo.clases;
import java.time.LocalDate;
import ito.poo.app.MyApp;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Cuenta implements Comparable<Cuenta>{
	
	Calendar calendario = new GregorianCalendar();
	int anio = calendario.get(Calendar.YEAR);
	int mes = calendario.get(Calendar.MONTH);
	int dia = calendario.get(Calendar.DAY_OF_MONTH);
	MyApp my = new MyApp();

	private int NumeroCuenta;
	private String nombrecliente; 
	private LocalDate fechadeapertura; 
	private int saldo;
	private LocalDate últimaactualización;
	
	public Cuenta(int numeroCuenta, String nombrecliente, LocalDate fechadeapertura, int saldo, LocalDate Ultimaactualizacion) {
		super();
		NumeroCuenta = numeroCuenta;
		this.nombrecliente = nombrecliente;
		this.fechadeapertura = fechadeapertura;
		this.saldo = saldo;
		this.últimaactualización = Ultimaactualizacion;
	}
	
	public boolean retiro(int valor, int numero) {
		int contador=0, posicion=0, result=0;
		
		for(contador=0; contador<my.cuentas.size(); contador++) {
			if(numero!=my.cuentas.get(contador).getNumeroCuenta()) {
				posicion = contador+1;
				contador = my.cuentas.size();
				result =1;
			}
		}
		if(result==0) {
			
			System.out.println("Su numero de cuenta no existe");
			return false;
			
		}
		if(valor<=my.cuentas.get(posicion).getSaldo()) {
			
			result = my.cuentas.get(posicion).getSaldo() - valor;
			my.cuentas.set(posicion, (new Cuenta(my.cuentas.get(posicion).getNumeroCuenta(), my.cuentas.get(posicion).getNombrecliente(), my.cuentas.get(posicion).getFechadeapertura(), result, LocalDate.of(anio, mes, dia))));
			return true;
			
		}else {
			
			System.out.println("No tiene suficiente dinero");
			return false;
			
		}
		
	}
	
	public void deposito(int valor, int numero) {
		
		int contador=0, posicion=0, result=0;
		
		for(contador=0; contador<my.cuentas.size(); contador++) {
			if(numero!=my.cuentas.get(contador).getNumeroCuenta()) {
				posicion = contador+1;
				contador = my.cuentas.size();
				result =1;
			}
		}
		if(result==0) {
			
			System.out.println("Su numero de cuenta no existe");
			
		}else {
			
			my.cuentas.set(posicion, (new Cuenta(my.cuentas.get(posicion).getNumeroCuenta(), my.cuentas.get(posicion).getNombrecliente(), my.cuentas.get(posicion).getFechadeapertura(), my.cuentas.get(posicion).getSaldo()+valor, LocalDate.of(anio, mes, dia))));
			System.out.println("Su saldo ha sido actualizado a: "+my.cuentas.get(posicion).getSaldo());
			
		}
		
	}

	public int getNumeroCuenta() {
		return NumeroCuenta;
	}

	public void setNumeroCuenta(int numeroCuenta) {
		NumeroCuenta = numeroCuenta;
	}

	public String getNombrecliente() {
		return nombrecliente;
	}

	public void setNombrecliente(String nombrecliente) {
		this.nombrecliente = nombrecliente;
	}

	public LocalDate getFechadeapertura() {
		return fechadeapertura;
	}

	public void setFechadeapertura(LocalDate fechadeapertura) {
		this.fechadeapertura = fechadeapertura;
	}

	public int getSaldo() {
		return saldo;
	}

	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}

	public LocalDate getÚltimaactualización() {
		return últimaactualización;
	}

	public void setÚltimaactualización(LocalDate últimaactualización) {
		this.últimaactualización = últimaactualización;
	}

	@Override
	public String toString() {
		return "Cuenta [NumeroCuenta=" + NumeroCuenta + ", "
				+ (nombrecliente != null ? "nombrecliente=" + nombrecliente + ", " : "")
				+ (fechadeapertura != null ? "fechadeapertura=" + fechadeapertura + ", " : "") + "saldo=" + saldo + ", "
				+ (últimaactualización != null ? "últimaactualización=" + últimaactualización : "") + "]\n";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + NumeroCuenta;
		result = prime * result + ((fechadeapertura == null) ? 0 : fechadeapertura.hashCode());
		result = prime * result + ((nombrecliente == null) ? 0 : nombrecliente.hashCode());
		result = prime * result + saldo;
		result = prime * result + ((últimaactualización == null) ? 0 : últimaactualización.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cuenta other = (Cuenta) obj;
		if (NumeroCuenta != other.NumeroCuenta)
			return false;
		if (fechadeapertura == null) {
			if (other.fechadeapertura != null)
				return false;
		} else if (!fechadeapertura.equals(other.fechadeapertura))
			return false;
		if (nombrecliente == null) {
			if (other.nombrecliente != null)
				return false;
		} else if (!nombrecliente.equals(other.nombrecliente))
			return false;
		if (saldo != other.saldo)
			return false;
		if (últimaactualización == null) {
			if (other.últimaactualización != null)
				return false;
		} else if (!últimaactualización.equals(other.últimaactualización))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(Cuenta o) {
		// TODO Auto-generated method stub
		if(this.NumeroCuenta!=o.getNumeroCuenta())
			return this.NumeroCuenta-o.getNumeroCuenta();
		else if(this.nombrecliente.compareTo(o.getNombrecliente())!=0)
			return this.nombrecliente.compareTo(o.getNombrecliente());
		else if(this.fechadeapertura.compareTo(o.getFechadeapertura())!=0)
			return this.fechadeapertura.compareTo(o.getFechadeapertura());
		return this.saldo-o.getSaldo();
	}
	
}
